package ru.bakhtiyarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentsCommand extends AbstractCommand {

    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> arguments = bootstrap.getArguments();
        for (@NotNull final AbstractCommand command : arguments)
            System.out.println(command.arg());
    }

}
