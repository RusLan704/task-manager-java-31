package ru.bakhtiyarov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionEndpoint;
import ru.bakhtiyarov.tm.service.SessionService;

@Component
public final class LogoutCommand extends AbstractCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout user in program";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSessionAll(sessionService.getSession());
        sessionService.clearSession();
        System.out.println("[OK]");
    }

}
