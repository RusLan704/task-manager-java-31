package ru.bakhtiyarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.SessionEndpoint;

@Component
public class PortInfoCommand extends AbstractCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;
    
    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "port-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display server port.";
    }

    @Override
    public void execute() {
        System.out.println("[PORT]");
        @NotNull SessionDTO session = sessionService.getSession();
        @NotNull final Integer port = sessionEndpoint.getServerPort(session);
        System.out.println(port);
        System.out.println("[OK]");
    }

}