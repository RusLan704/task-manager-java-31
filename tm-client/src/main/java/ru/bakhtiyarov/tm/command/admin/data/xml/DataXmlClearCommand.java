package ru.bakhtiyarov.tm.command.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataXmlClearCommand extends AbstractCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear xml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML CLEAR]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.clearXml(session);
        System.out.println("[OK]");
    }

}