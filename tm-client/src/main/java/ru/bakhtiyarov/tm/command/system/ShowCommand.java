package ru.bakhtiyarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

@Component
public final class ShowCommand extends AbstractCommand {

    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@NotNull AbstractCommand command : commands) System.out.println(command.name());
    }

}