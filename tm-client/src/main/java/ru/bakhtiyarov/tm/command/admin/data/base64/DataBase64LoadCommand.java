package ru.bakhtiyarov.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataBase64LoadCommand extends AbstractCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 data from file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.loadBase64(session);
        System.out.println("[OK]");
    }

}
