package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public class TaskClearByProjectNameCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;
    
    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-clear-by-project-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks by project name.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        taskEndpoint.removeAllByUserIdAndProjectName(session, projectName);
        System.out.println("[OK]");
    }

}