package ru.bakhtiyarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        @Nullable final ProjectDTO project = projectEndpoint.removeProjectOneByName(session, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
