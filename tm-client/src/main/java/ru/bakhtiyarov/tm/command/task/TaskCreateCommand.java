package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class TaskCreateCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        taskEndpoint.createTaskByNameDescription(session, projectName, taskName, description);
        System.out.println("[OK]");
    }

}
