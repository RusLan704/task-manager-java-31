package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public final class TaskListCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @NotNull SessionDTO session = sessionService.getSession();
        final List<TaskDTO> tasks = taskEndpoint.findAllTasksBySession(session);
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}
