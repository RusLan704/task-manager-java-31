package ru.bakhtiyarov.tm.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataYamlClearCommand extends AbstractCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear yaml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML CLEAR]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.clearYaml(session);
        System.out.println("[OK]");
    }

}