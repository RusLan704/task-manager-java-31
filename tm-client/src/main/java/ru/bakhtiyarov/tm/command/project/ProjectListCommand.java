package ru.bakhtiyarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

import java.util.List;

@Component
public final class ProjectListCommand extends AbstractCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull SessionDTO session = sessionService.getSession();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsBySession(session);
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
        }
        System.out.println("[OK]");
    }

}