package ru.bakhtiyarov.tm.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataYamlLoadCommand extends AbstractCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from YAML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML LOAD]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.loadYaml(session);
        System.out.println("[OK]");
    }

}

