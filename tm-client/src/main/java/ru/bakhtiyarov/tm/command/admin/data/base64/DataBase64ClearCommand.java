package ru.bakhtiyarov.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataBase64ClearCommand extends AbstractCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;
    
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.clearBase64(session);
        System.out.println("[OK]");
    }

}
