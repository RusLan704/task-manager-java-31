package ru.bakhtiyarov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.exception.system.UnknownArgumentException;
import ru.bakhtiyarov.tm.exception.system.UnknownCommandException;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public final class Bootstrap {

    @Getter
    @Autowired
    private List<AbstractCommand> commandList;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private void initCommandList() {
        for (@Nullable final AbstractCommand command : commandList) {
            if (command == null) continue;
            commands.put(command.name(), command);
            arguments.put(command.arg(), command);
        }
    }

    public void run(@Nullable final String... args) {
        System.out.println("Welcome to task manager!!!");
        initCommandList();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        if(arg == null) return false;
        parseArg(arg);
        return true;
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
            command.execute();
    }

    @SneakyThrows
    private void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
            argument.execute();
    }

    private static void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    @NotNull
    public Collection<AbstractCommand> getCommands(){
         return commands.values();
    }

    @NotNull
    public Collection<AbstractCommand> getArguments(){
        return arguments.values();
    }

}