package ru.bakhtiyarov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.bakhtiyarov.tm.endpoint.*;

@Configuration
@ComponentScan("ru.bakhtiyarov.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint() {
        return sessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        return projectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        return taskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public AdminDataEndpointService adminDataEndpointService() {
        return new AdminDataEndpointService();
    }

    @Bean
    @NotNull
    public AdminDataEndpoint adminDataEndpoint() {
        return adminDataEndpointService().getAdminDataEndpointPort();
    }

    @Bean
    @NotNull
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @Bean
    @NotNull
    public AdminUserEndpoint adminUserEndpoint() {
        return adminUserEndpointService().getAdminUserEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        return userEndpointService().getUserEndpointPort();
    }

}
