package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidProjectException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidUserException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable String projectName, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull Task task = new Task(name);
        task.setUser(user);
        task.setProject(project);
        persist(task);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable String projectName, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull Task task = new Task(name, description);
        task.setUser(user);
        task.setProject(project);
        persist(task);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull ITaskRepository repository = getRepository();
        Task task = new Task();
        try {
            repository.begin();
            task = repository.removeOneByIndex(userId, index);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull ITaskRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull ITaskRepository repository = getRepository();
        return repository.findAllByUserIdAndProjectName(userId, projectName);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull ITaskRepository repository = getRepository();
        return repository.findAllByUserId();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskRepository repository = getRepository();
        Task task = new Task();
        try {
            repository.begin();
            task = repository.removeOneById(userId, id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskRepository repository = getRepository();
        Task task = new Task();
        try {
            repository.begin();
            task = repository.removeOneByName(userId, name);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return task;
    }

    @Override
    public List<Task> removeAll() {
        @NotNull ITaskRepository repository = getRepository();
        List<Task> tasks = new ArrayList<>();
        try {
            repository.begin();
            tasks = repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return tasks;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> removeAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull ITaskRepository repository = getRepository();
        List<Task> tasks = new ArrayList<>();
        try {
            repository.begin();
            tasks = repository.removeAllByUserIdAndProjectId(userId, projectName);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return tasks;
    }


    @Nullable
    @Override
    @SneakyThrows
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return merge(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskRepository repository = getRepository();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return merge(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull ITaskRepository repository = getRepository();
        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskRepository repository = getRepository();
        return repository.findOneByName(userId, name);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

}
