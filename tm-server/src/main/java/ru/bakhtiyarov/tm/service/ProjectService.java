package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidUserException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IUserService userService;
    
    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = new Project(name);
        project.setUser(user);
        persist(project);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @NotNull Project project = new Project(name, description);
        project.setUser(user);
        persist(project);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull IProjectRepository repository = getRepository();
        Project project = new Project();
        try {
            repository.begin();
            project = repository.removeOneByIndex(userId, index);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull IProjectRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull IProjectRepository repository = getRepository();
        return repository.findAllByUserId();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IProjectRepository repository = getRepository();
        Project project = new Project();
        try {
            repository.begin();
            project = repository.removeOneById(userId, id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull IProjectRepository repository = getRepository();
        Project project = new Project();
        try {
            repository.begin();
            project = repository.removeOneByName(userId, name);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return project;
    }

    @Override
    public List<Project> removeAll() {
        @NotNull IProjectRepository repository = getRepository();
        List<Project> projects = new ArrayList<>();
        try {
            repository.begin();
            projects = repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return merge(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return merge(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IProjectRepository repository = getRepository();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull IProjectRepository repository = getRepository();
        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull IProjectRepository repository = getRepository();
        return repository.findOneByName(userId, name);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

}