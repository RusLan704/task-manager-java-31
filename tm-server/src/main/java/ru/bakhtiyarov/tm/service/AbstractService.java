package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.api.service.IService;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.exception.invalid.InvalidTaskException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Override
    public void addAll(@Nullable List<E> records) {
        if (records == null) return;
        @NotNull IRepository<E> repository = getRepository();
        try {
            repository.begin();
            repository.addAll(records);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public E persist(@Nullable final E record) {
        if (record == null) throw new InvalidTaskException();
        @NotNull IRepository<E> repository = getRepository();
        E newRecord = null;
        try {
            repository.begin();
            newRecord = repository.persist(record);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return newRecord;
    }

    @Override
    @Nullable
    @SneakyThrows
    public E merge(@Nullable final E record) {
        if (record == null) return null;
        @NotNull IRepository<E> repository = getRepository();
        E newRecord = null;
        try {
            repository.begin();
            newRecord = repository.merge(record);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return newRecord;
    }

}