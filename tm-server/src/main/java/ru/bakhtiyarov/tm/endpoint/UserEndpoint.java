package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.IUserEndpoint;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.api.service.converter.ISessionConverter;
import ru.bakhtiyarov.tm.api.service.converter.IUserConverter;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class UserEndpoint implements IUserEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ISessionConverter sessionConverter;

    @Autowired
    private IUserConverter userConverter;

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO createUserByLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.create(login, password);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO createUserByLoginPasswordEmail(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {

        @Nullable User user = userService.create(login, password, email);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO createUserByLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final Role role
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, role);
        @Nullable User user = userService.create(login, password, role);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.findByLogin(login);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.findById(session.getUserId());
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.updatePassword(session.getUserId(), password);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserEmail(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.updateUserEmail(session.getUserId(), email);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "firstName", partName = "firstName") final String firstName
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.updateUserFirstName(session.getUserId(), firstName);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserLastName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "lastName", partName = "lastName") final String lastName
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.updateUserLastName(session.getUserId(), lastName);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "middleName", partName = "middleName") final String middleName
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.updateUserMiddleName(session.getUserId(), middleName);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable User user = userService.updateUserLogin(session.getUserId(), login);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

}
