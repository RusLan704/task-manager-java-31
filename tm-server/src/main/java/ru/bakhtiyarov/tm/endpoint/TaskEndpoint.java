package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.ITaskEndpoint;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.converter.ISessionConverter;
import ru.bakhtiyarov.tm.api.service.converter.ITaskConverter;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
@Controller
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private ISessionConverter sessionConverter;

    @Autowired
    private ITaskConverter taskConverter;

    @Override
    @WebMethod
    @SneakyThrows
    public void createTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        taskService.create(session.getUserId(), projectName, taskName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTaskByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        taskService.create(session.getUserId(), projectName, taskName, description);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable List<Task> tasks = taskService.findAll(sessionDTO.getUserId());
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void clearAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        taskService.removeAll(sessionDTO.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.findOneById(session.getUserId(), id);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.findOneByIndex(session.getUserId(), index);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.findOneByName(session.getUserId(), name);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO removeTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.removeOneByIndex(session.getUserId(), index);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO removeTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.removeOneById(session.getUserId(), id);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO removeTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.removeOneByName(session.getUserId(), name);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.updateTaskById(session.getUserId(), id, name, description);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Task task = taskService.updateTaskByIndex(session.getUserId(), index, name, description);
        @Nullable TaskDTO taskDTO = taskConverter.toDTO(task);
        return taskDTO;
    }

    @Override
    @NotNull
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllByUserIdAndProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable List<Task> tasks = taskService
                .findAllByUserIdAndProjectName(sessionDTO.getUserId(), projectName);
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> removeAllByUserIdAndProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable List<Task> tasks = taskService
                .removeAllByUserIdAndProjectName(sessionDTO.getUserId(), projectName);
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

}
