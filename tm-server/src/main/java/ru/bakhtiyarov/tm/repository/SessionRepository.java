package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean removeAll(String userId) {
        @NotNull final List<Session> sessions = findAll(userId);
        sessions.forEach(entityManager::remove);
        return true;
    }

    @NotNull
    @Override
    public List<Session> findAll(String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId",userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> removeAll() {
        @NotNull final List<Session> sessions = findAll();
        sessions.forEach(this::remove);
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

}
