package ru.bakhtiyarov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.api.endpoint.*;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.service.UserService;

import javax.xml.ws.Endpoint;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private UserService userService;

    @Autowired
    @NotNull
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAdminDataEndpoint adminDataEndpoint;

    @NotNull
    @Autowired
    private IAdminUserEndpoint adminUserEndpoint;


    public void run() {
        initUsers();
        initEndpoint();
        System.out.println("SERVER START!!!");
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initEndpoint() {
        registryEndpoint(taskEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(adminDataEndpoint);
        registryEndpoint(adminUserEndpoint);
        registryEndpoint(sessionEndpoint);
    }

    private void registryEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}