package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.AbstractEntityDTO;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    void addAll(@Nullable List<E> tasks);

    @Nullable
    E persist(@Nullable E record);

    @Nullable
    E merge(@Nullable E record);

}
